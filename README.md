# Luncheons

## Get started
1. Install global dependencies `express`, `mongodb`, `mongoose` via `yarn` or `npm`
2. Install project dependencies via `yarn` or `npm`
3. Do mongodb setup (see mongodb's documentation for help on setup)
3. See the `Development server` section to run in development mode or the `Production server` section for production mode

## Project structure overview
.
├── app			-> Angular frontend (has its own README.md)
└── server	-> Express server routes and mongoose models
   	├── models
    └── routes

## Development server

1. Start mongodb with `mongod`
2. Run `npm run server_dev` or `npm start` to start the development server
3. Go to `/app` and see `Development server` in README.md for starting the Angular app in development mode

## Production server

1. Start mongodb with `mongod`
2. Run `npm run deploy_prod` to build frontend and starting production server