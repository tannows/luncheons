var express = require("express");
var router = express.Router();
var path = require("path");

var restaurantRoutes = require("./restaurant");
var lunchRoutes = require("./lunch");
var bossRoutes = require("./boss");
var attendantRoutes = require("./attendants");

router.route("/restaurants").get(restaurantRoutes.getRestaurants);
router.route("/restaurants").post(restaurantRoutes.createRestaurant);
router.route("/restaurants/:restaurantId").get(restaurantRoutes.getRestaurant);
router.route("/restaurants/:restaurantId").put(restaurantRoutes.updateRestaurant);
router.route("/restaurants/:restaurantId").delete(restaurantRoutes.deleteRestaurant);

router.route("/lunches").get(lunchRoutes.getLunches);
router.route("/lunches").post(lunchRoutes.createLunch);
router.route("/lunches/reset").patch(lunchRoutes.resetLunches);
router.route("/lunches/:lunchId").get(lunchRoutes.getLunch);
router.route("/lunches/:lunchId").put(lunchRoutes.updateLunch);
router.route("/lunches/:lunchId").delete(lunchRoutes.deleteLunch);

router.route("/lunches/attendants").post(attendantRoutes.addAttendant);
router.route("/lunches/:lunchId/attendants").get(attendantRoutes.getAttendants);
router.route("/lunches/:lunchId/attendants/:bossId").delete(attendantRoutes.removeAttendant);

router.route("/bosses").get(bossRoutes.getBosses);
router.route("/bosses").post(bossRoutes.createBoss);
router.route("/bosses/:bossId").get(bossRoutes.getBoss);
router.route("/bosses/:bossId").put(bossRoutes.updateBoss);
router.route("/bosses/:bossId").delete(bossRoutes.deleteBoss);

// Api overview
router.route("/").get(function(req, res) {
	res.status(404).send({ message: "404" });
	//res.status(200).sendFile(path.resolve(__dirname + "/api.html"));
});

module.exports = router;