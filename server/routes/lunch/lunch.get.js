var Lunch = require("../../models/lunch.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Find a single lunch with a lunchId
  Lunch.findById(req.params.lunchId, function(err, lunch) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Lunch not found with id " + req.params.lunchId});
      }
      return res.status(500).send({message: "Error retrieving lunch with id " + req.params.lunchId});
    }

    if(!lunch) {
      return res.status(404).send({message: "Lunch not found with id " + req.params.lunchId});
    }

    res.send(lunch);
  });
};