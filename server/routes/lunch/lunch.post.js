var Lunch = require("../../models/lunch.model.js");
var headers = require("../headers");
var mongoose = require("mongoose");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Create a new lunch vist
  if(!req.body.date) {
    return res.status(400).send({message: "Date can not be empty"});
  }
  if(!req.body.bossId) {
    return res.status(400).send({message: "Boss can not be empty"});
  }

  var lunch = new Lunch({
    _id: new mongoose.Types.ObjectId(),
  	date: req.body.date || null,
    time: req.body.time || "12:00",
    theme: req.body.theme || null,
    restaurant: req.body.restaurantId || null,
    boss: req.body.bossId || null,
    inactive: req.body.inactive || false,
    takeaway: req.body.takeaway || false,
    takeawayTo: req.body.takeaway ? "5aba437fd374e6649b407335" : null // The office
  });

  lunch.save(function(err, data) {
    if(err) {
      res.status(500).send({message: "Some error occurred while creating the Lunch."});
    } else {
      res.send(data);
    }
  });
};