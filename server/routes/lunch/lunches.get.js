var Lunch = require("../../models/lunch.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  var query = {};
  if (req.query.inactive) {
    query = {"inactive": {$eq: req.query.inactive}};
  }

  // Retrieve and return all lunches from the database.
  Lunch
  .find(query)
  .populate({ path: "boss", select: "name" })
  .populate({ path: "restaurant", select: "name address city zipCode menuLink" })
  .populate({ path: "takeawayTo", select: "name address city zipCode menuLink" })
  .exec(function(err, lunches){
    if(err) {
      res.status(500).send({message: "Some error occurred while retrieving lunches."});
    } else {
      res.send(lunches);
    }
  });
};