var Lunch = require("../../models/lunch.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

    // Update a lunch identified by the lunchId in the request
	Lunch.findById(req.params.lunchId, function(err, lunch) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Lunch not found with id " + req.params.lunchId});
      }
      return res.status(500).send({message: "Error finding lunch with id " + req.params.lunchId});
    }

    if(!lunch) {
      return res.status(404).send({message: "Lunch not found with id " + req.params.lunchId});
    }

    lunch.date = req.body.date;
    lunch.time = req.body.time || "12:00";
    lunch.theme = req.body.theme || null;
    lunch.restaurant = req.body.restaurantId || null;
    lunch.boss = req.body.bossId;
    lunch.inactive = req.body.inactive;
    lunch.takeaway = req.body.takeaway || false;
    lunch.takeawayTo = req.body.takeaway ? "5aba437fd374e6649b407335" : null // The office

    lunch.save(function(err, data){
      if(err) {
        res.status(500).send({message: "Could not update lunch with id " + req.params.lunchId});
      } else {
        res.send(data);
      }
    });
  });
};