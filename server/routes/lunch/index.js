module.exports.getLunches = require("./lunches.get.js");
module.exports.resetLunches = require("./lunches.reset.js");
module.exports.createLunch = require("./lunch.post.js");
module.exports.getLunch = require("./lunch.get.js");
module.exports.updateLunch = require("./lunch.put.js");
module.exports.deleteLunch = require("./lunch.delete.js");