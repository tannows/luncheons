var Lunch = require("../../models/lunch.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  var query = {
    _id: { $in: req.body }
  };

  Lunch.update(query, { inactive: true }, { multi: true }, function(err, raw) {
    if (err) {
      res.status(500).send({message: "Could not reset lunches " + err});
    } else {
      res.status(200).send({message: "Ok"});
    }
  });
};