var Lunch = require("../../models/lunch.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

    // Delete a lunch with the specified lunchId in the request
	Lunch.findByIdAndRemove(req.params.lunchId, function(err, lunch) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Lunch not found with id " + req.params.lunchId});
      }
      return res.status(500).send({message: "Could not delete lunch with id " + req.params.lunchId});
    }

    if(!lunch) {
      return res.status(404).send({message: "Lunch not found with id " + req.params.lunchId});
    }

    res.send({message: "Lunch deleted successfully!"})
  });
};