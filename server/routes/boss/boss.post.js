var Boss = require("../../models/boss.model.js");
var headers = require("../headers");
var mongoose = require("mongoose");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Create and Save a new Boss
	if(!req.body.name) {
    return res.status(400).send({message: "Boss can not be empty"});
  }

  var boss = new Boss({
    _id: new mongoose.Types.ObjectId(),
  	name: req.body.name,
    inactive: req.body.inactive || false,
    bot: req.body.bot || false
  });

  boss.save(function(err, data) {
    if(err) {
      if (err.code === 11000) {
        res.status(400).send({message: "Boss with this name already exists."});
      } else {
        res.status(500).send({message: "Some error occurred while creating the Boss."});
      }
    } else {
      res.send(data);
    }
  });
};