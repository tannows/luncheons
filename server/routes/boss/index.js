module.exports.getBosses = require("./bosses.get.js");
module.exports.createBoss = require("./boss.post.js");
module.exports.getBoss = require("./boss.get.js");
module.exports.updateBoss = require("./boss.put.js");
module.exports.deleteBoss = require("./boss.delete.js");