var Boss = require("../../models/boss.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  var query = {};
  if (req.query.inactive) {
    query = {"inactive": {$eq: req.query.inactive}};
  }

  // Retrieve and return all bosses from the database.
  Boss.find(query, function(err, bosses){
    if(err) {
      res.status(500).send({message: "Some error occurred while retrieving bosses."});
    } else {
      res.send(bosses);
    }
  });
};