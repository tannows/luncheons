var Boss = require("../../models/boss.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Update a boss identified by the bossId in the request
	Boss.findById(req.params.bossId, function(err, boss) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Boss not found with id " + req.params.bossId});
      }
      return res.status(500).send({message: "Error finding boss with id " + req.params.bossId});
    }

    if(!boss) {
      return res.status(404).send({message: "Boss not found with id " + req.params.bossId});
    }

    boss.name = req.body.name;
    boss.inactive = req.body.inactive;
    boss.bot = req.body.bot;

    boss.save(function(err, data){
      if(err) {
        res.status(500).send({message: "Could not update boss with id " + req.params.bossId});
      } else {
        res.send(data);
      }
    });
  });
};