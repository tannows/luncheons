var Boss = require("../../models/boss.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Delete a boss with the specified bossId in the request
	Boss.findByIdAndRemove(req.params.bossId, function(err, boss) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Boss not found with id " + req.params.bossId});
      }
      return res.status(500).send({message: "Could not delete boss with id " + req.params.bossId});
    }

    if(!boss) {
      return res.status(404).send({message: "Boss not found with id " + req.params.bossId});
    }

    res.send({message: "Boss deleted successfully!"})
  });
};