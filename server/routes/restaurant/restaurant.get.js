const Restaurant = require("../../models/restaurant.model.js");
const headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Find a single restaurant with a restaurantId
	Restaurant.findById(req.params.restaurantId, function(err, restaurant) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Restaurant not found with id " + req.params.restaurantId});
      }
      return res.status(500).send({message: "Error retrieving restaurant with id " + req.params.restaurantId});
    }

    if(!restaurant) {
      return res.status(404).send({message: "Restaurant not found with id " + req.params.restaurantId});
    }

    res.send(restaurant);
  });
};