module.exports.getRestaurants = require("./restaurants.get.js");
module.exports.createRestaurant = require("./restaurant.post.js");
module.exports.getRestaurant = require("./restaurant.get.js");
module.exports.updateRestaurant = require("./restaurant.put.js");
module.exports.deleteRestaurant = require("./restaurant.delete.js");