const Restaurant = require("../../models/restaurant.model.js");
const headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Delete a restaurant with the specified restaurantId in the request
	Restaurant.findByIdAndRemove(req.params.restaurantId, function(err, restaurant) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Restaurant not found with id " + req.params.restaurantId});
      }
      return res.status(500).send({message: "Could not delete restaurant with id " + req.params.restaurantId});
    }

    if(!restaurant) {
      return res.status(404).send({message: "Restaurant not found with id " + req.params.restaurantId});
    }

    res.send({message: "Restaurant deleted successfully!"})
  });
};