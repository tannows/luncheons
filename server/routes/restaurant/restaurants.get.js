var Restaurant = require("../../models/restaurant.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  var query = {};
  if (req.query.inactive) {
    query = {"inactive": {$eq: req.query.inactive}};
  }

  // Retrieve and return all restaurants from the database.
  Restaurant.find(query, function(err, restaurants){
    if(err) {
      res.status(500).send({message: "Some error occurred while retrieving restaurants."});
    } else {
      res.send(restaurants);
    }
  });
};