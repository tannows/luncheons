var Restaurant = require("../../models/restaurant.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Update a restaurant identified by the restaurantId in the request
	Restaurant.findById(req.params.restaurantId, function(err, restaurant) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Restaurant not found with id " + req.params.restaurantId});
      }
      return res.status(500).send({message: "Error finding restaurant with id " + req.params.restaurantId});
    }

    if(!restaurant) {
      return res.status(404).send({message: "Restaurant not found with id " + req.params.restaurantId});
    }

    restaurant.name = req.body.name;
    restaurant.address = req.body.address;
    restaurant.city = req.body.city;
    restaurant.zipCode = req.body.zipCode;
    restaurant.menuLink = req.body.menuLink;
    restaurant.inactive = req.body.inactive;
    restaurant.bot = req.body.bot;

    restaurant.save(function(err, data){
      if(err) {
        res.status(500).send({message: "Could not update restaurant with id " + req.params.restaurantId});
      } else {
        res.send(data);
      }
    });
  });
};