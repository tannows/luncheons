var Restaurant = require("../../models/restaurant.model.js");
var headers = require("../headers");
var mongoose = require("mongoose");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Create and Save a new Restaurant
	if(!req.body.name) {
    return res.status(400).send({message: "Restaurant can not be empty"});
  }

  var restaurant = new Restaurant({
    _id: new mongoose.Types.ObjectId(),
  	name: req.body.name,
    address: req.body.address,
    city: req.body.city,
    zipCode: req.body.zipCode,
    menuLink: req.body.menuLink || null,
    inactive: req.body.inactive || false,
    bot: req.body.bot || false
    });

  restaurant.save(function(err, data) {
    if(err) {
      if (err.code === 11000) {
        res.status(400).send({message: "Restaurant with this name already exists."});
      } else {
        res.status(500).send({message: "Some error occurred while creating the Restaurant."});
      }
    } else {
      res.send(data);
    }
  });
};