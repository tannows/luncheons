var Attendant = require("../../models/attendant.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  var query = {};
  if (req.params.lunchId) {
    query = {"lunch": req.params.lunchId};
  }

  // Retrieve and return all attendants from the database.
  Attendant
  .find(query)
  .populate({ path: "boss", select: "name" })
  .populate({ path: "lunch", select: "" })
  .exec(function(err, attendants){
    if(err) {
      res.status(500).send({message: "Some error occurred while retrieving attendants."});
    } else {
      res.send(attendants);
    }
  });
};