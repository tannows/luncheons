module.exports.getAttendants = require("./attendants.get.js");
module.exports.addAttendant = require("./attendant.post.js");
module.exports.removeAttendant = require("./attendant.delete.js");