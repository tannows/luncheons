var Attendant = require("../../models/attendant.model.js");
var headers = require("../headers");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  var query = {
    $and: [
      { "lunch": req.params.lunchId },
      { "boss": req.params.bossId }
    ]
  };

  // Delete a attendant with the specified bossId in the request
	Attendant.findOneAndRemove(query, function(err, attendant) {
    if(err) {
      if(err.kind === "ObjectId") {
        return res.status(404).send({message: "Attendant not found with id " + req.params.bossId});
      }
      return res.status(500).send({message: "Could not delete attendant with id " + req.params.bossId});
    }

    if(!attendant) {
      return res.status(404).send({message: "Attendant not found with id " + req.params.bossId});
    }

    res.send({message: "Attendant deleted successfully!"})
  });
};