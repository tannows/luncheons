var Attendant = require("../../models/attendant.model.js");
var headers = require("../headers");
var mongoose = require("mongoose");

module.exports = function(req, res) {
  // Set headers
  headers(res);

  // Create and Save a new Attendant
	if(!req.body.bossId || !req.body.lunchId) {
    return res.status(400).send({message: "Attendant can not be empty"});
  }

  var attendant = new Attendant({
    _id: new mongoose.Types.ObjectId(),
  	boss: req.body.bossId,
    lunch: req.body.lunchId
  });

  attendant.save(function(err, data) {
    if(err) {
      if (err.code === 11000) {
        res.status(400).send({message: "Attendant already attending this lunch."});
      } else {
        res.status(500).send({message: "Some error occurred while creating the Attendant."});
      }
    } else {
      res.send(data);
    }
  });
};