const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const LunchSchema = Schema({
    date: { type: String, required: true },
    time: { type: String },
    theme: String,
    restaurant: { type: Schema.Types.ObjectId, ref: "Restaurant" },
    boss: { type: Schema.Types.ObjectId, ref: "Boss", required: true },
    inactive: Boolean,
    takeaway: Boolean,
    takeawayTo: { type: Schema.Types.ObjectId, ref: "Restaurant" }
}, {
	timestamps: true
});

module.exports = mongoose.model("Lunch", LunchSchema, "lunches");
