const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AttendantSchema = Schema({
    boss: { type: Schema.Types.ObjectId, ref: "Boss", required: true },
    lunch: { type: Schema.Types.ObjectId, ref: "Lunch", required: true }
}, {
	timestamps: true
});

AttendantSchema.index({ "boss": 1, "lunch": 1 }, { "unique": true });

module.exports = mongoose.model("Attendant", AttendantSchema);
