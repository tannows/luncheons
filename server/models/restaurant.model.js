const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RestaurantSchema = Schema({
		_id: Schema.Types.ObjectId,
    name: { type: String, unique: true, required: true, dropDups: true },
    address: String,
    city: String,
    zipCode: String,
    menuLink: String,
    inactive: Boolean,
    bot: Boolean
}, {
	timestamps: true
});

module.exports = mongoose.model("Restaurant", RestaurantSchema);