const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BossSchema = Schema({
		_id: Schema.Types.ObjectId,
    name: { type: String, unique: true, required: true, dropDups: true },
    inactive: Boolean,
    bot: Boolean
}, {
	timestamps: true
});

module.exports = mongoose.model("Boss", BossSchema);