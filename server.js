// Get dependencies
var express = require("express");
var path = require("path");
var http = require("http");
var https = require("https");
var bodyParser = require("body-parser");
var cors = require("cors");
var helmet = require("helmet");
var fs = require("fs");
var morgan = require("morgan");
var compression = require("compression");
var app = express();
var mongoose = require("mongoose");

var routes = require("./server/routes/api");

// Create servers
var httpPort = 3002;
var httpsPort = 3443;

var key = ''; // Add real key file
var cert = ''; // Add real cert file

if (process.env.NODE_ENV === "development") {
	key = fs.readFileSync("certificates/key.pem", "utf-8");
  cert = fs.readFileSync("certificates/cert.pem", "utf-8");
}

var httpServer = http.createServer(app);
var httpsServer = https.createServer({ key, cert }, app);

// Database setup
mongoose.connect("mongodb://localhost:27017/luncheons", { useNewUrlParser: true } );

// Handle the connection event
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));

db.once("open", function() {
  console.log("DB connection alive");
});

// Compress requests
app.use(compression());

// Add logging
app.use(morgan("common"));

// Support parsing of application/json type post data
app.use(bodyParser.json());

// Support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

// Enable cors if not production environment
if (process.env.NODE_ENV === "development") {
	app.use(cors());
}

// Add helmet
app.use(helmet());

// Point static path to dist
app.use(express.static(path.join(__dirname, "app/dist")));

// Set our api routes
app.use("/api", routes);

// Catch all other routes and return the index file
app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "app/dist/index.html"));
});

// Listen on provided port, on all network interfaces.
httpServer.listen(httpPort, function() {
	console.log("HTTP running on port " + httpPort + " in " + process.env.NODE_ENV + " mode");
});
httpsServer.listen(httpsPort, function() {
	console.log("HTTPS running on port " + httpsPort + " in " + process.env.NODE_ENV + " mode");
});