# Luncheons

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Get started
1. Install global dependencies `@angular/cli` and `typescript` via `yarn` or `npm`
2. Install project dependencies via `yarn` or `npm`
3. See the `Development server` section to run in development mode or the `Build` section for production mode

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Project structure overview
.
├── dist
└── src
    ├── app
 		|		├── components -> More than one components/directives/enums/interfaces/services that are related and are better suited
 		| 	|									together in one Module instead of separately in the "shared" directory
		|		├── shared 		 -> Smaller (mostly self sustained) components/directives/enums/interfaces/services
 		|		└── views 		 -> Components/Modules with its own route
   	├── assets
    ├── environments
    └── styles

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
