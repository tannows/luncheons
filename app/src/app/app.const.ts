import { InjectionToken } from '@angular/core';

// Storage keys
export interface StorageConst {
  APP_PREFIX: string;
  USER_ID: string;
}

export const storageKeys = {
  APP_PREFIX: 'luncheons',
  USER_ID: 'user'
};

export const STORAGE_KEYS = new InjectionToken<StorageConst>('storageKeys');

export const APP_CONSTANTS_PROVIDERS = [
  { provide: STORAGE_KEYS, useValue: storageKeys }
];
