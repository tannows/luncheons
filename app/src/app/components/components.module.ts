import { NgModule } from '@angular/core';
import { DialogModule } from './dialog';
import { LoadingSpinnerModule } from './loading-spinner';

/*
 * Module to gather a collection of different common modules/components that have to use
 * parts from SharedModule and can therefore not be declared there itself
 */


@NgModule({
  imports: [
    DialogModule,
    LoadingSpinnerModule
  ],
  exports: [
    DialogModule,
    LoadingSpinnerModule
  ],
  providers: [
    DialogModule,
    LoadingSpinnerModule
  ]
})
export class ComponentsModule {
}
