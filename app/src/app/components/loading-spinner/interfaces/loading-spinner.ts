import { LoadingSpinnerState } from '../enums';

export interface LoadingSpinner {
	message: string;
	state: LoadingSpinnerState;
}
