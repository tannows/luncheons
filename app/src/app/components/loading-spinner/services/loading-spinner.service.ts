﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { LoadingSpinner } from '../interfaces';
import { LoadingSpinnerState } from '../enums';

/*
 * Service to handle displaying loading-spinner component
 */

@Injectable()
export class LoadingSpinnerService {
  public spinner: any;
  private _loadingSpinnerObserver: Observer<LoadingSpinner>;
  private _visible: boolean;

  constructor() {
    this.spinner = Observable.create((observer: Observer<LoadingSpinner>) => {
      this._loadingSpinnerObserver = observer;
    }).publish();
    this.spinner.connect();
  }

  public get isLoading(): boolean {
    return this._visible;
  }

  public show(message?: string) {
    setTimeout(() => { // To avvoid ExpressionChangesAfterItHasBeenCheckedError
      this._visible = true;

      if (this._loadingSpinnerObserver) {
        this._loadingSpinnerObserver.next({ message, state: LoadingSpinnerState.Show });
      }
    }, 0);
  }

  public hide(): void {
    setTimeout(() => { // To avvoid ExpressionChangesAfterItHasBeenCheckedError
      this._visible = false;

      if (this._loadingSpinnerObserver) {
        this._loadingSpinnerObserver.next({ message: undefined, state: LoadingSpinnerState.Hide });
      }
    }, 0);
  }
}
