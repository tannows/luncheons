import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';

import { LoadingSpinnerComponent } from './components';
import { LoadingSpinnerService } from './services';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
  	LoadingSpinnerComponent
  ],
  exports: [
  	LoadingSpinnerComponent
  ],
  providers: [LoadingSpinnerService]
})
export class LoadingSpinnerModule { }
