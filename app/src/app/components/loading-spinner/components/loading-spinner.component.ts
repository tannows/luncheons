import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoadingSpinnerService } from '../services';
import { LoadingSpinnerState } from '../enums';

/*
 * Component to display a loadingspinner
 */

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.scss']
})
export class LoadingSpinnerComponent implements OnInit, OnDestroy {
	public message: string;
	private spinnerSub: Subscription;
	private _visible: boolean;

	@HostBinding('class.visible')
  private get visible(): boolean {
    return this._visible;
  }

  constructor(
  	private loadingSpinner: LoadingSpinnerService
  ) {
  	//
  }

  public ngOnInit() {
  	this.spinnerSub = this.loadingSpinner.spinner.subscribe((spinner) => {
  		this._visible = spinner.state === LoadingSpinnerState.Show;
  		this.message = spinner.message;
  	});
  }

  public ngOnDestroy() {
  	if (this.spinnerSub) {
  		this.spinnerSub.unsubscribe();
  	}
  }
}
