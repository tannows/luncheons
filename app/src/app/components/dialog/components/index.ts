export { DialogComponent } from './dialog.component';
export { BossDialogComponent } from './boss';
export { RestaurantDialogComponent } from './restaurant';
export { LunchDialogComponent } from './lunch';
export { LunchAttendantDialogComponent } from './lunch-attendant';
