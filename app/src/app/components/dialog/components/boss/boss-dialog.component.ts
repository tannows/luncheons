import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Boss, BossRequest } from '@shared/interfaces';

@Component({
  selector: 'app-dialog-boss',
  templateUrl: './boss-dialog.component.html'
})
export class BossDialogComponent implements OnInit {
  public form: FormGroup;
  public boss: Boss;

  constructor(public dialogRef: MatDialogRef<BossDialogComponent>) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      inactive: new FormControl(false),
      bot: new FormControl(false)
    });
  }

  public ngOnInit() {
    if (this.boss) {
      this.form.get('name').setValue(this.boss.name);
      this.form.get('inactive').setValue(this.boss.inactive);
      this.form.get('bot').setValue(this.boss.bot);
    }
  }

  public saveBoss() {
    if (this.form.invalid) {
      return;
    }
    const data: BossRequest = {
      name: this.form.get('name').value,
      inactive: this.form.get('inactive').value,
      bot: this.form.get('bot').value
    };
    this.dialogRef.close({ action: 'save', data });
  }
}
