import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Restaurant, RestaurantRequest } from '@shared/interfaces';

@Component({
  selector: 'app-dialog-restaurant',
  templateUrl: './restaurant-dialog.component.html'
})
export class RestaurantDialogComponent implements OnInit {
  public form: FormGroup;
  public restaurant: Restaurant;

  constructor(public dialogRef: MatDialogRef<RestaurantDialogComponent>) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      address: new FormControl(),
      city: new FormControl(),
      zipCode: new FormControl(),
      menuLink: new FormControl(),
      inactive: new FormControl(false),
      bot: new FormControl(false)
    });
  }

  public ngOnInit() {
    if (this.restaurant) {
      this.form.get('name').setValue(this.restaurant.name);
      this.form.get('address').setValue(this.restaurant.address);
      this.form.get('city').setValue(this.restaurant.city);
      this.form.get('zipCode').setValue(this.restaurant.zipCode);
      this.form.get('menuLink').setValue(this.restaurant.menuLink);
      this.form.get('inactive').setValue(this.restaurant.inactive);
      this.form.get('bot').setValue(this.restaurant.bot);
    }
  }

  public saveRestaurant() {
    if (this.form.invalid) {
      return;
    }
    const data: RestaurantRequest = {
      name: this.form.get('name').value,
      address: this.form.get('address').value,
      city: this.form.get('city').value,
      zipCode: this.form.get('zipCode').value,
      menuLink: this.form.get('menuLink').value,
      inactive: this.form.get('inactive').value,
      bot: this.form.get('bot').value
    };
    this.dialogRef.close({ action: 'save', data });
  }
}
