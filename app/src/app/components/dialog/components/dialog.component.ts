import { MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';
import { DialogButton } from '../interfaces';

@Component({
  selector: 'app-dialog-default',
  template: `
    <div class="dialog-content">
      <div class="dialog-title">
        <div><strong>{{ title | translate }}</strong></div>
      </div>
      <div class="dialog-icon" *ngIf="icon">
        <i class="icon {{ icon }}"></i>
      </div>
      <div class="dialog-message" *ngIf="text">
        <div [innerHTML]="text | translate"></div>
      </div>
      <div class="dialog-buttons">
        <button *ngFor="let button of buttons"
                class="btn {{ button.className ? button.className : '' }}"
                (click)="dialogRef.close({ action: button.action })">
          {{ button.text | translate }}
        </button>
      </div>
    </div>
  `
})
export class DialogComponent {
  public title: string;
  public text: string;
  public icon: string;
  public buttons: DialogButton[];

  constructor(public dialogRef: MatDialogRef<DialogComponent>) { }
}
