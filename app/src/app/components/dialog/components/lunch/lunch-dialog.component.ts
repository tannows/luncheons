import { MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Restaurant, Boss, Lunch, LunchRequest } from '@shared/interfaces';

@Component({
  selector: 'app-dialog-lunch',
  templateUrl: './lunch-dialog.component.html'
})
export class LunchDialogComponent implements OnInit {
  public restaurants: Restaurant[];
  public bosses: Boss[];
  public suggestedNextBoss: Boss;
  public form: FormGroup;
  public lunch: Lunch;

  constructor(public dialogRef: MatDialogRef<LunchDialogComponent>) {
    this.form = new FormGroup({
      date: new FormControl('', [Validators.required]),
      time: new FormControl('12:00'),
      restaurantId: new FormControl(),
      theme: new FormControl(),
      restaurant: new FormControl(),
      bossId: new FormControl('', [Validators.required]),
      boss: new FormControl('', [Validators.required]),
      inactive: new FormControl(false),
      takeaway: new FormControl(false)
    });
  }

  public ngOnInit() {
    if (this.lunch) {
      const r: any = this.lunch.restaurant ? this.lunch.restaurant : undefined;
      const b: any = this.lunch.boss ? this.lunch.boss : undefined;
      this.form.get('date').setValue(new Date(this.lunch.date));
      this.form.get('time').setValue(this.lunch.time);
      this.form.get('theme').setValue(this.lunch.theme);
      this.form.get('restaurantId').setValue(r ? r._id : '');
      this.form.get('restaurant').setValue(r ? r.name : '');
      this.form.get('bossId').setValue(b ? b._id : '');
      this.form.get('boss').setValue(b ? b.name : '');
      this.form.get('inactive').setValue(this.lunch.inactive);
      this.form.get('takeaway').setValue(this.lunch.takeaway);
    }

    this.form.get('restaurantId').valueChanges.subscribe((id) => {
      if (id) {
        this.form.get('restaurant').setValue(this.getRestaurant(id).name);
      } else {
        this.form.get('restaurant').setValue('');
      }
    });

    this.form.get('bossId').valueChanges.subscribe((id) => {
      if (id) {
        this.form.get('boss').setValue(this.getBoss(id).name);
      } else {
        this.form.get('boss').setValue('');
      }
    });
  }

  public saveLunch() {
    if (this.form.invalid) {
      return;
    }
    const date = new Date(this.form.get('date').value);
    const data: LunchRequest = {
      date: `${date.getFullYear()}-${this.datePrefixing(date.getMonth() + 1)}-${this.datePrefixing(date.getDate())}`,
      time: this.form.get('time').value || '12:00',
      theme: this.form.get('theme').value || '',
      restaurantId: this.form.get('restaurantId').value || '',
      bossId: this.form.get('bossId').value || '',
      inactive: this.form.get('inactive').value || false,
      takeaway: this.form.get('takeaway').value || false
    };
    this.dialogRef.close({ action: 'save', data });
  }

  private getRestaurant(id: string) {
    return this.restaurants.find((r) => {
      return r._id === id;
    });
  }

  private getBoss(id: string) {
    return this.bosses.find((b) => {
      return b._id === id;
    });
  }

  private datePrefixing(dateToPrefix: number): string {
    if (dateToPrefix < 10) {
      return `0${dateToPrefix}`;
    }
    return `${dateToPrefix}`;
  }
}
