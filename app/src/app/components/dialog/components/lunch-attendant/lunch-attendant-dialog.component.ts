import { MatDialogRef } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Boss, Lunch, LunchAttendant, LunchAttendantRequest } from '@shared/interfaces';
import { StorageService } from '@shared/services/storage.service';
import { STORAGE_KEYS, StorageConst } from '@app/app.const';

@Component({
  selector: 'app-dialog-lunch-attendant',
  templateUrl: './lunch-attendant-dialog.component.html'
})
export class LunchAttendantDialogComponent implements OnInit {
  public lunchId: string;
  public attendants: LunchAttendant[];
  public bosses: Boss[];
  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<LunchAttendantDialogComponent>,
    private storageService: StorageService,
    @Inject(STORAGE_KEYS)
    public storageKeys: StorageConst
  ) {
    this.form = new FormGroup({
      bossId: new FormControl('', [Validators.required]),
      attending: new FormControl(true)
    });
  }

  public ngOnInit() {
    const userId = this.storageService.getFromStorage(this.storageKeys.USER_ID, 'localStorage');
    if (userId) {
      this.form.get('bossId').setValue(userId);
    }
  }

  public saveLunchAttendant() {
    if (this.form.invalid) {
      return;
    }
    const data: LunchAttendantRequest = {
      bossId: this.form.get('bossId').value,
      attending: this.form.get('attending').value,
      lunchId: this.lunchId
    };
    this.storageService.setToStorage(this.storageKeys.USER_ID, data.bossId, 'localStorage');
    this.dialogRef.close({ action: 'save', data });
  }
}
