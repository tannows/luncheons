﻿export { DialogModule } from './dialog.module';
export {
  DialogButton,
  DialogResponse
} from './interfaces';
export {
  DialogComponent,
  BossDialogComponent,
  RestaurantDialogComponent,
  LunchDialogComponent,
  LunchAttendantDialogComponent
} from './components';
export { DialogService } from './services';
