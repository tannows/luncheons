export interface DialogResponse {
	action: string;
	data: any;
}
