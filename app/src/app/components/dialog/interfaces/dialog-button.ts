export interface DialogButton {
  text: string;
  className?: string;
  action?: string;
}
