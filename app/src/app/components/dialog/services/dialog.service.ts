import { Injectable, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {
  MatDialogRef,
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import {
  DialogComponent,
  BossDialogComponent,
  RestaurantDialogComponent,
  LunchDialogComponent,
  LunchAttendantDialogComponent
} from '../components';
import {
  DialogButton,
  DialogResponse
} from '../interfaces';
import { Restaurant, Boss, Lunch, LunchAttendant } from '@shared/interfaces';

@Injectable()
export class DialogService {
  private rootContainerRef: ViewContainerRef;

  constructor(
    private dialog: MatDialog
  ) {
    //
  }

  public initDialogService(viewContainerRef: ViewContainerRef) {
    this.rootContainerRef = viewContainerRef;
  }

  public defaultDialog(
    title: string,
    text: string,
    icon: string,
    buttons: DialogButton[]
  ): Observable<DialogResponse> {
    const config = this.getConfig();
    let dialogRef: MatDialogRef<DialogComponent>;
    dialogRef = this.dialog.open(DialogComponent, config);
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.text = text;
    dialogRef.componentInstance.icon = icon;
    dialogRef.componentInstance.buttons = buttons;

    return dialogRef.afterClosed();
  }

  public bossDialog(boss?: Boss): Observable<DialogResponse> {
    const config = this.getConfig();
    let dialogRef: MatDialogRef<BossDialogComponent>;
    dialogRef = this.dialog.open(BossDialogComponent, config);
    dialogRef.componentInstance.boss = boss;

    return dialogRef.afterClosed();
  }

  public restaurantDialog(restaurant?: Restaurant): Observable<DialogResponse> {
    const config = this.getConfig();
    let dialogRef: MatDialogRef<RestaurantDialogComponent>;
    dialogRef = this.dialog.open(RestaurantDialogComponent, config);
    dialogRef.componentInstance.restaurant = restaurant;

    return dialogRef.afterClosed();
  }

  public lunchDialog(
    lunch: Lunch,
    restaurants: Restaurant[],
    bosses: Boss[],
    suggestedNextBoss: Boss
  ): Observable<DialogResponse> {
    const config = this.getConfig();
    let dialogRef: MatDialogRef<LunchDialogComponent>;
    dialogRef = this.dialog.open(LunchDialogComponent, config);
    dialogRef.componentInstance.restaurants = restaurants;
    dialogRef.componentInstance.bosses = bosses;
    dialogRef.componentInstance.lunch = lunch;
    dialogRef.componentInstance.suggestedNextBoss = suggestedNextBoss;

    return dialogRef.afterClosed();
  }

  public lunchAttendantDialog(
    lunchId: string,
    attendants: LunchAttendant[],
    bosses: Boss[]
  ): Observable<DialogResponse> {
    const config = this.getConfig();
    let dialogRef: MatDialogRef<LunchAttendantDialogComponent>;
    dialogRef = this.dialog.open(LunchAttendantDialogComponent, config);
    dialogRef.componentInstance.lunchId = lunchId;
    dialogRef.componentInstance.attendants = attendants;
    dialogRef.componentInstance.bosses = bosses;

    return dialogRef.afterClosed();
  }

  private getConfig(disableClose: boolean = false): MatDialogConfig {
    return {
      viewContainerRef: this.rootContainerRef,
      disableClose
    };
  }
}
