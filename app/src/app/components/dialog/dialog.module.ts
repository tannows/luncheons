import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';

import {
  DialogComponent,
  BossDialogComponent,
  RestaurantDialogComponent,
  LunchDialogComponent,
  LunchAttendantDialogComponent
} from './components';
import { DialogService } from './services';

@NgModule({
  imports: [SharedModule],
  declarations: [
    DialogComponent,
    BossDialogComponent,
    RestaurantDialogComponent,
    LunchDialogComponent,
    LunchAttendantDialogComponent
  ],
  entryComponents: [
    DialogComponent,
    BossDialogComponent,
    RestaurantDialogComponent,
    LunchDialogComponent,
    LunchAttendantDialogComponent
  ],
  exports: [
    DialogComponent,
    BossDialogComponent,
    RestaurantDialogComponent,
    LunchDialogComponent,
    LunchAttendantDialogComponent
  ],
  providers: [
    DialogService
  ]
})
export class DialogModule { }
