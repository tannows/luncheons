import { NgModule } from '@angular/core';
import { LuncheonModule } from './luncheon';
import { NoContentModule } from './no-content';
/*
 * Module to gather view modules
 */

@NgModule({
  imports: [
    LuncheonModule,
    NoContentModule
  ],
  exports: [
    LuncheonModule,
    NoContentModule
  ]
})
export class ViewsModule { }
