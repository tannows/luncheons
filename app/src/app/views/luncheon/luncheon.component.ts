import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { DialogService } from '@components/dialog/services';
import { LoadingSpinnerService } from '@components/loading-spinner/services';
import {
  Restaurant,
  Lunch,
  Boss,
  LunchAttendant
} from '@shared/interfaces';
import { ApiService } from '@shared/services';
import {
  RestaurantAvailablePipe,
  ClosestToNowPipe,
  OrderByPipe,
  ActivePipe,
  SuggestNextBossPipe,
  InThePastPipe
} from '@shared/pipes';

@Component({
  selector: 'app-luncheon',
  templateUrl: './luncheon.component.html',
  styleUrls: ['./luncheon.component.scss']
})
export class LuncheonComponent implements OnInit {
  public restaurants: Restaurant[];
  public lunches: Lunch[];
  public bosses: Boss[];
  public nextLunch: Lunch;
  public attendants: any;
  public showVisitedRestaurants: boolean;
  public showInactiveBosses: boolean;
  public showInactiveLunches: boolean;
  public tab: string;

  constructor(
    private dialogService: DialogService,
    private apiService: ApiService,
    private restaurantAvailablePipe: RestaurantAvailablePipe,
    private closestToNowPipe: ClosestToNowPipe,
    private orderByPipe: OrderByPipe,
    private activePipe: ActivePipe,
    private suggestNextBossPipe: SuggestNextBossPipe,
    private inThePastPipe: InThePastPipe,
    private translateService: TranslateService,
    private loadingSpinner: LoadingSpinnerService
  ) {
    this.tab = 'lunch';
    this.attendants = {};
  }

  public ngOnInit() {
    this.loadData(true);
  }

  public addBoss(boss?: Boss) {
    this.dialogService.bossDialog(boss).subscribe((res) => {
      if (res && res.action === 'save') {
        const method = boss ? 'updateBoss' : 'createBoss';
        const body = { data: res.data, id: boss ? boss._id : undefined };
        this.apiService[method](body).subscribe((response) => {
          this.loadData();
        }, (error) => {
          console.log('Create boss failed', error);
        });
      }
    });
  }

  public addRestaurant(restaurant?: Restaurant) {
    this.dialogService.restaurantDialog(restaurant).subscribe((res) => {
      if (res && res.action === 'save') {
        const method = restaurant ? 'updateRestaurant' : 'createRestaurant';
        const body = { data: res.data, id: restaurant ? restaurant._id : undefined };
        this.apiService[method](body).subscribe((response) => {
          this.loadData();
        }, (error) => {
          console.log('Create restaurant failed', error);
        });
      }
    });
  }

  public addLunch(lunch?: Lunch) {
    const isLunchInThePast = lunch ? this.inThePastPipe.transform(lunch) : false;
    // If new lunch or upcoming, only allow choice from available restaurants
    let restaurants = lunch && isLunchInThePast ?
      this.restaurants : this.restaurantAvailablePipe.transform(this.restaurants, this.lunches, undefined, lunch);
    // If new lunch, only allow choice from active restaurants
    restaurants = lunch ? restaurants : this.activePipe.transform(restaurants);
    // If new lunch, only allow choice from active bosses
    const bosses = lunch && isLunchInThePast ? this.bosses : this.activePipe.transform(this.bosses);
    const suggestedNextBoss = lunch ? null : this.suggestNextBossPipe.transform(
      this.bosses,
      this.lunches
    );

    this.dialogService.lunchDialog(
      lunch,
      restaurants,
      bosses,
      suggestedNextBoss
    ).subscribe((res) => {
      if (res && res.action === 'save') {
        const method = lunch ? 'updateLunch' : 'createLunch';
        const body = { data: res.data, id: lunch ? lunch._id : undefined };
        this.apiService[method](body).subscribe((response) => {
          this.loadData();
        }, (error) => {
          console.log('Create lunch failed', error);
        });
      }
    });
  }

  public addLunchAttendant(lunchId: string) {
    this.dialogService.lunchAttendantDialog(
      lunchId,
      this.attendants,
      this.activePipe.transform(this.bosses)
    ).subscribe((res) => {
      if (res && res.action === 'save') {
        const method = res.data.attending ? 'addLunchAttendant' : 'removeLunchAttendant';
        const body = {
          data: res.data,
          id: res.data.attending ? undefined : lunchId,
          bossId: res.data.attending ? undefined : res.data.bossId,
        };
        this.apiService[method](body).subscribe((response) => {
          this.getAttendants(lunchId);
        }, (error) => {
          console.log('Add lunch attedant failed', error);
        });
      }
    });
  }

  public deleteLunch(id: string) {
    this.dialogService.defaultDialog(
      'DELETE_LUNCH_TITLE',
      'DELETE_LUNCH_TEXT',
      '',
      [
        { text: 'CANCEL', action: 'cancel' },
        { text: 'YES_DELETE', action: 'delete', className: 'btn-danger' }
      ]
    ).subscribe((res) => {
      if (res.action === 'delete') {
        this.loadingSpinner.show('DELETING');
        this.apiService.deleteLunch(id).subscribe(() => {
          this.loadingSpinner.hide();
            // Done
            this.loadData();
          }, (error) => {
            console.log('Failed to delete lunch', error);
            this.loadingSpinner.hide();
          });
      }
    });
  }

  public resetVisitedRestaurants() {
    this.dialogService.defaultDialog(
      'RESET_VISITED_RESTAURANTS',
      'RESET_VISITED_RESTAURANTS_TEXT',
      '',
      [
        { text: 'CANCEL', action: 'cancel' },
        { text: 'YES_RESET', action: 'reset', className: 'btn-danger' }
      ]
    ).subscribe((res) => {
      if (res.action === 'reset') {
        const ids = [];
        for (const lunch of this.lunches) {
          ids.push(lunch._id);
        }
        this.loadingSpinner.show('RESETTING_RESTAURANTS');
        this.apiService.resetLunches(ids).subscribe(() => {
          this.loadingSpinner.hide();
            // Done
            this.loadData();
          }, (error) => {
            console.log('Failed to reset lunches', error);
            this.loadingSpinner.hide();
          });
      }
    });
  }

  private getBosses() {
    this.apiService.getBosses().subscribe((bosses) => {
      this.bosses = bosses;
    }, (error) => {
      console.log('Failed to get bosses', error);
    });
  }

  private getRestaurants() {
    this.apiService.getRestaurants().subscribe((restaurants) => {
      this.restaurants = restaurants;
    }, (error) => {
      console.log('Failed to get restaurants', error);
    });
  }

  private getLunches() {
    this.apiService.getLunches().subscribe((lunches) => {
      this.lunches = lunches;
      this.setNextLunch(lunches);
    }, (error) => {
      console.log('Failed to get lunches', error);
    });
  }

  private getAttendants(lunchId?: string) {
    const observables = [];
    if (lunchId) {
      observables.push(this.apiService.getLunchAttendants(lunchId));
    } else {
      for (const lunch of this.lunches) {
        observables.push(this.apiService.getLunchAttendants(lunch._id));
      }
    }

    Observable.forkJoin(observables).subscribe((response) => {
      response.forEach((attendants: LunchAttendant[]) => {
        if (attendants && attendants.length > 0) {
          const id = attendants[0].lunch._id;
          this.attendants[id] = attendants;
        }
      });
    }, (error) => {
      console.log('getAttendants', error);
    });
  }

  private loadData(onInit: boolean = false) {
    this.loadingSpinner.show('LOADING');

    Observable.forkJoin([
      this.apiService.getBosses(),
      this.apiService.getRestaurants(),
      this.apiService.getLunches()
    ]).subscribe((response) => {
      this.bosses = <Boss[]>response[0];
      this.restaurants = <Restaurant[]>response[1];
      this.lunches = <Lunch[]>response[2];
      this.setNextLunch(this.lunches);
      if (onInit) {
        this.getAttendants();
      }

      this.loadingSpinner.hide();
    }, (error) => {
      console.log('init error', error);
      this.loadingSpinner.hide();
    });
  }

  private setNextLunch(lunches: Lunch[]) {
    // Filter out active lunches
    let l = this.activePipe.transform(lunches);
    if (l.length === 0) { return; }

    // Filter out lunches in the past
    l = this.closestToNowPipe.transform(l);
    if (l.length === 0) { return; }

    // Sort by date
    l = this.orderByPipe.transform(l, 'date');

    // Set upcoming lunch
    this.nextLunch = l[0];
  }
}
