import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { LunchAttendant } from '@shared/interfaces';

@Component({
  selector: 'app-luncheon-attendants',
  template: `
  <div class="attendants-anchor" (click)="$event.stopImmediatePropagation(); visible = !visible" #clickTarget>
    <ng-content></ng-content>
  </div>
  <div class="attendants-content"
       [class.visible]="visible"
       (click)="$event.stopImmediatePropagation()"
       appClickOutside
       [clickTargets]="[clickTarget]"
       [skipIfHidden]="true"
       (clickedOutside)="visible = false">
    <h4>{{ 'ATTENDING_BOSSES' | translate }}</h4>
    <ul>
      <li *ngFor="let attendant of attendants | orderBy:['boss', 'name']">
        {{ attendant.boss?.name }}
      </li>
    </ul>
  </div>
  `,
  styleUrls: ['./attendants.component.scss']
})
export class AttendantsComponent implements OnInit {
  @Input()
  public attendants: LunchAttendant[];
  public visible: boolean;

  constructor() {
    //
  }

  public ngOnInit() {
    //
  }
}
