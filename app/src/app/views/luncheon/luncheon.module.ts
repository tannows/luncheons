import { NgModule, } from '@angular/core';
import { SharedModule } from '@shared';
import { DialogModule } from '@components';
import { LuncheonComponent } from './luncheon.component';
import { AttendantsComponent } from './attendants';
import { NextLuncheonComponent } from './next-luncheon';

@NgModule({
  imports: [
  	SharedModule,
  	DialogModule
  ],
  declarations: [
  	LuncheonComponent,
  	AttendantsComponent,
    NextLuncheonComponent
  ],
  exports: [
  	LuncheonComponent,
  	AttendantsComponent,
    NextLuncheonComponent
  ]
})
export class LuncheonModule { }
