import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Lunch, LunchAttendant } from '@shared/interfaces';

@Component({
  selector: 'app-luncheon-next-luncheon',
  templateUrl: './next-luncheon.component.html',
  styleUrls: ['./next-luncheon.component.scss']
})
export class NextLuncheonComponent implements OnInit {
  @Input()
  public nextLuncheon: Lunch;
  @Input()
  public attendants: LunchAttendant[];
  @Output()
  public addAttendant = new EventEmitter();

  constructor() {
    //
  }

  public ngOnInit() {
    //
    console.log(this.nextLuncheon);
  }
}
