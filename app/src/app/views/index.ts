/*
 * Collection of modules/components with its own route
 */

export { ViewsModule } from './views.module';
export {
	LuncheonModule,
	LuncheonComponent,
	AttendantsComponent,
	NextLuncheonComponent
} from './luncheon';
export {
	NoContentModule,
	NoContentComponent
} from './no-content';
