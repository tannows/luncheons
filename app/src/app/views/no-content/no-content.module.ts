import { NgModule, } from '@angular/core';

import { SharedModule } from '@shared';
import { NoContentComponent } from './no-content.component';

@NgModule({
  imports: [SharedModule],
  declarations: [NoContentComponent],
  exports: [NoContentComponent]
})
export class NoContentModule { }
