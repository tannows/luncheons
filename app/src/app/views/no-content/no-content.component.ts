import { Component } from '@angular/core';

@Component({
  selector: 'app-no-content',
  template: `
    <section class="page-container">
			<header class="page-header">
				<h1>{{ 'PAGE_NOT_FOUND' | translate }}</h1>
			</header>
		</section>
  `
})
export class NoContentComponent {

}
