import { Routes } from '@angular/router';
import {
  LuncheonComponent,
  NoContentComponent
} from '@views';

export const ROUTES: Routes = [
  {
    path: '',
    component: LuncheonComponent
  },
  {
    path: 'luncheon',
    component: LuncheonComponent
  },
  {
    path: '**',
    component: NoContentComponent
  }
];
