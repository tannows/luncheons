import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, LOCALE_ID } from '@angular/core';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';
import { DateAdapter } from '@angular/material';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {
  TranslateModule,
  TranslateLoader,
  MissingTranslationHandler
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AppMaterialModule } from './app-material.module';
import { ROUTES } from './app.routes';
import { APP_CONSTANTS_PROVIDERS } from './app.const';

import { ComponentsModule } from '@components';
import { ViewsModule } from '@views';
import { SharedModule, SwedishNativeDateAdapter } from '@shared';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, `/assets/i18n/`, '.json');
}

import { registerLocaleData } from '@angular/common';
import localeSv from '@angular/common/locales/sv';
import localeSvExtra from '@angular/common/locales/extra/sv';
registerLocaleData(localeSv, 'sv', localeSvExtra);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    ComponentsModule,

    ViewsModule,

    AppMaterialModule.forRoot(),
    SharedModule.forRoot(),
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    ...APP_CONSTANTS_PROVIDERS,
    { provide: DateAdapter, useClass: SwedishNativeDateAdapter },
    { provide: LOCALE_ID, useValue: 'sv' },
    AppMaterialModule.forRoot().providers,
  	SharedModule.forRoot().providers,
    ComponentsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
