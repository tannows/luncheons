import {
  Component,
  ViewEncapsulation,
  ViewContainerRef
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from '@components/dialog/services';
import './operators';

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private viewContainerRef: ViewContainerRef,
    private dialogService: DialogService,
    private translateService: TranslateService
  ) {
    this.dialogService.initDialogService(this.viewContainerRef);
    this.initTranslateService();
  }

  private initTranslateService() {
    let userLang = navigator.language;

    // Regex for supported languages
    userLang = /(sv-SE)/gi.test(userLang) ? userLang : 'sv-SE';

    this.translateService.addLangs(['sv-SE', 'en-US']);

    this.translateService.setDefaultLang('sv-SE');
    this.translateService.use(userLang);
  }
}
