import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import {
  AppMaterialModule
} from '@app/app-material.module';

/*
 * Module to gather a collection common modules, smaller components, services etc
 */

import {
  GoogleMapsLinkComponent
} from './components';

import {
  ClickOutsideDirective
} from './directives';

import {

} from './guards';

import {
  RestaurantAvailablePipe,
  OrderByPipe,
  InThePastPipe,
  ClosestToNowPipe,
  ActivePipe,
  IndexPipe,
  SuggestNextBossPipe
} from './pipes';

import {
  DataService,
  ApiService,
  StorageService
} from './services';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    AppMaterialModule
  ],
  declarations: [
    GoogleMapsLinkComponent,

    ClickOutsideDirective,

    RestaurantAvailablePipe,
    OrderByPipe,
    InThePastPipe,
    ClosestToNowPipe,
    ActivePipe,
    IndexPipe,
    SuggestNextBossPipe
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    AppMaterialModule,

    GoogleMapsLinkComponent,

    ClickOutsideDirective,

    RestaurantAvailablePipe,
    OrderByPipe,
    InThePastPipe,
    ClosestToNowPipe,
    ActivePipe,
    IndexPipe,
    SuggestNextBossPipe
  ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        TranslateService,
        DataService,
        ApiService,
        StorageService,

        DatePipe,
        RestaurantAvailablePipe,
        OrderByPipe,
        InThePastPipe,
        ClosestToNowPipe,
        ActivePipe,
        IndexPipe,
        SuggestNextBossPipe
      ]
    };
  }
}
