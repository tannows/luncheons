import { Injectable, Inject } from '@angular/core';
import { STORAGE_KEYS, StorageConst } from '@app/app.const';

type StorageType = 'localStorage' | 'sessionStorage';

@Injectable()
export class StorageService {
  private _prefix: string;

  constructor(
    @Inject(STORAGE_KEYS)
    protected storageKeys: StorageConst
  ) {
    if (!localStorage) {
      throw new Error('Current browser does not support Local Storage');
    }
    if (!sessionStorage) {
      throw new Error('Current browser does not support Session Storage');
    }
    this.setAppPrefix(this.storageKeys.APP_PREFIX);
  }

  public setAppPrefix(prefix: string) {
    this._prefix = `${prefix}-`;
  }

  public getFromStorage(key: string, type: StorageType) {
    const storage = this.getStorage(type);
    const item = storage.getItem(this.getPrefixedKey(key));
    if (!item) {
      return null;
    }

    try {
      return JSON.parse(item);
    } catch (e) {
      return item;
    }
  }

  public setToStorage(key: string, data: any, type: StorageType) {
    const value = JSON.stringify(data);
    const storage = this.getStorage(type);

    try {
      storage.setItem(this.getPrefixedKey(key), value);
    } catch (e) {
      throw new Error(
        `Item with key: "${this.getPrefixedKey(key)}" could not be set to ${type}`
      );
    }
  }

  public removeFromStorage(key: string, type: StorageType) {
    const storage = this.getStorage(type);
    try {
      storage.removeItem(this.getPrefixedKey(key));
    } catch (e) {
      throw new Error(
        `Item with key: "${this.getPrefixedKey(key)}" could not be removed from $type}`
      );
    }
  }

  public getKeysForStorage(type: StorageType): string[] {
    const prefixLength: number = this._prefix.length;
    const keys: string[] = [];
    const storage = this.getStorage(type);

    for (const key in storage) {
      // Only return keys that are for this app
      if (key.substr(0, prefixLength) === this._prefix) {
        keys.push(key.substr(prefixLength));
      }
    }
    return keys;
  }

  // Remove all data for this app from local storage
  // Also optionally takes a regular expression string and removes the matching key-value pairs
  // Example use: storageService.clearAll();
  // Should be used mostly for development purposes
  public clearAllFromStorage(
    regularExpression: RegExp,
    type: StorageType
  ) {
    // Setting both regular expressions independently
    // Empty strings result in catchall RegExp
    const prefixRegex = !!this._prefix ? new RegExp('^' + this._prefix) : new RegExp('');
    const testRegex = !!regularExpression ? new RegExp(regularExpression) : new RegExp('');
    const prefixLength: number = this._prefix.length;

    const storage = this.getStorage(type);
    for (const key in storage) {
      // Only remove items that are for this app and match the regular expression
      if (prefixRegex.test(key) && testRegex.test(key.substr(prefixLength))) {
        try {
          this.removeFromStorage(key.substr(prefixLength), type);
        } catch (e) {
          throw new Error(
            `Item with key: "${this.getPrefixedKey(key)}" could not be removed from ${type}`
          );
        }
      }
    }

    return true;
  }

  public lengthOfStorage(type: StorageType) {
    let count = 0;
    const storage = this.getStorage(type);

    for (let i = 0; i < storage.length; i++) {
      if (storage.key(i).indexOf(this._prefix) === 0 ) {
        count++;
      }
    }
    return count;
  }

  protected getStorage(type: StorageType) {
    switch (type) {
      case 'localStorage':
        return localStorage;
      case 'sessionStorage':
        return sessionStorage;
      default:
        return;
    }
  }

  protected get prefix() {
    return this._prefix;
  }

  private getPrefixedKey(key: string) {
    return `${this._prefix}${key}`;
  }
}
