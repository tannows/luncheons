import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { environment } from '@environments';
import { DialogService } from '@components/dialog/services';
import { DataService } from './data.service';
import {
  Restaurant,
  RestaurantRequest,
  Boss,
  BossRequest,
  Lunch,
  LunchRequest
} from '@shared/interfaces';
/*
 * Service to handle communications with given api config
 */

@Injectable()
export class ApiService extends DataService {
  private _api: string;
  private _apiKey: string;
  private _token: string;

  constructor(
    http: HttpClient,
    dialogService: DialogService,
    router: Router
  ) {
    super(http, dialogService, router);
    this._api = environment.api;
  }

  /* Internal Config
   ========================================================================== */

  public set token(token: string) {
    this._token = token;
  }

  /* Restaurants
   ========================================================================== */

  public getRestaurants(): Observable<Restaurant[]> {
    return this.GET(`${this._api}/api/restaurants`);
  }

  public getRestaurant(id: string): Observable<Restaurant> {
    return this.GET(`${this._api}/api/restaurants/${id}`);
  }

  public createRestaurant(body: any): Observable<any> {
    return this.POST(`${this._api}/api/restaurants`, body.data);
  }

  public updateRestaurant(body: any): Observable<any> {
    return this.PUT(`${this._api}/api/restaurants/${body.id}`, body.data);
  }

  public deleteRestaurant(id: string): Observable<any> {
    return this.DELETE(`${this._api}/api/restaurants/${id}`);
  }

  /* Bosses
   ========================================================================== */

  public getBosses(): Observable<Boss[]> {
    return this.GET(`${this._api}/api/bosses`);
  }

  public getBoss(id: string): Observable<Boss> {
    return this.GET(`${this._api}/api/bosses/${id}`);
  }

  public createBoss(body: any): Observable<any> {
    return this.POST(`${this._api}/api/bosses`, body.data);
  }

  public updateBoss(body: any): Observable<any> {
    return this.PUT(`${this._api}/api/bosses/${body.id}`, body.data);
  }

  public deleteBoss(id: string): Observable<any> {
    return this.DELETE(`${this._api}/api/bosses/${id}`);
  }

  /* Lunches
   ========================================================================== */

  public getLunches(): Observable<Lunch[]> {
    return this.GET(`${this._api}/api/lunches`);
  }

  public resetLunches(body: any): Observable<any> {
    return this.PATCH(`${this._api}/api/lunches/reset`, body);
  }

  public getLunch(id: string): Observable<Lunch> {
    return this.GET(`${this._api}/api/lunches/${id}`);
  }

  public createLunch(body: any): Observable<any> {
    return this.POST(`${this._api}/api/lunches`, body.data);
  }

  public updateLunch(body: any): Observable<any> {
    return this.PUT(`${this._api}/api/lunches/${body.id}`, body.data);
  }

  public deleteLunch(id: string): Observable<any> {
    return this.DELETE(`${this._api}/api/lunches/${id}`);
  }

  public getLunchAttendants(lunchId: string) {
    return this.GET(`${this._api}/api/lunches/${lunchId}/attendants`);
  }

  public addLunchAttendant(body: any) {
    return this.POST(`${this._api}/api/lunches/attendants`, body.data);
  }

  public removeLunchAttendant(body: any) {
    return this.DELETE(`${this._api}/api/lunches/${body.id}/attendants/${body.bossId}`);
  }
}
