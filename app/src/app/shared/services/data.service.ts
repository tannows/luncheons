import { Injectable } from '@angular/core';
import {
  Response
} from '@angular/http';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse,
  HttpParams
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DialogService } from '@components/dialog/services';
import { ServerError } from '@shared/interfaces';

/*
 * Service to handle http requests
 */

@Injectable()
export class DataService {
  protected contentTypes = {
    json: 'application/json',
    x_www_form_urlencoded: 'application/x-www-form-urlencoded'
  };

  constructor(
    private http: HttpClient,
    private dialogService: DialogService,
    private router: Router
  ) {
    //
  }

  public GET(
    url: string,
    apiKey: string = null,
    token: string = null,
    params: HttpParams = null,
    skipErrorDialog: boolean = false
  ): Observable<any> {
    const headers = this.getHeaders(apiKey, token);
    return this.http.get(url, { headers, params })
      .map((res) => this.extractData(res))
      .catch((error: HttpErrorResponse) => {
        return Observable.throw(this.handleError(error, skipErrorDialog));
      });
  }

  public POST(
    url: string,
    data: any,
    apiKey: string = null,
    token: string = null,
    params: HttpParams = null,
    contentType: string = null,
    skipErrorDialog: boolean = false
  ): Observable<any> {
    const headers = this.getHeaders(apiKey, token, this.contentTypes.json);
    const body = this.getBody(data, contentType);
    return this.http.post(url, body, { headers, params })
      .map((res) => this.extractData(res))
      .catch((error: HttpErrorResponse) => {
        return Observable.throw(this.handleError(error, skipErrorDialog));
      });
  }

  public PUT(
    url: string,
    data: any,
    apiKey: string = null,
    token: string = null,
    params: HttpParams = null,
    contentType: string = null,
    skipErrorDialog: boolean = false
  ): Observable<any> {
    const headers = this.getHeaders(apiKey, token, this.contentTypes.json);
    const body = this.getBody(data, contentType);
    return this.http.put(url, body, { headers, params })
      .map((res) => this.extractData(res))
      .catch((error: HttpErrorResponse) => {
        return Observable.throw(this.handleError(error, skipErrorDialog));
      });
  }

  public PATCH(
    url: string,
    data: any,
    apiKey: string = null,
    token: string = null,
    params: HttpParams = null,
    contentType: string = null,
    skipErrorDialog: boolean = false
  ): Observable<any> {
    const headers = this.getHeaders(apiKey, token, this.contentTypes.json);
    const body = this.getBody(data, contentType);
    return this.http.patch(url, body, { headers, params })
      .map((res) => this.extractData(res))
      .catch((error: HttpErrorResponse) => {
        return Observable.throw(this.handleError(error, skipErrorDialog));
      });
  }

  public DELETE(
    url: string,
    apiKey: string = null,
    token: string = null,
    params: HttpParams = null,
    skipErrorDialog: boolean = false
  ): Observable<any> {
    const headers = this.getHeaders(apiKey, token);
    return this.http.delete(url, { headers, params })
      .map((res) => this.extractData(res))
      .catch((error: HttpErrorResponse) => {
        return Observable.throw(this.handleError(error, skipErrorDialog));
      });
  }

  private getHeaders(
    apiKey: string,
    token: string,
    contentType?: string
  ): HttpHeaders {
    let httpHeaders: any = new HttpHeaders();

    if (contentType) {
      httpHeaders = httpHeaders.set('Content-Type', contentType);
      httpHeaders = httpHeaders.set('Accept', contentType);
    }

    if (apiKey) {
      httpHeaders = httpHeaders.set('apiKey', 'apiKey');
    }

    if (token) {
      httpHeaders = httpHeaders.set('Authorization', `Bearer ${token}`);
    }


    return httpHeaders;
  }

  private getBody(data: any, contentType?: string): any {
    // Set default content type if none given
    if (!contentType) {
      contentType = this.contentTypes.json;
    }

    if (contentType === this.contentTypes.x_www_form_urlencoded) {
      return data;
    }
    return JSON.stringify(data);
  }

  private extractData(res: any): any {
    return res;
  }

   private handleError(error: HttpErrorResponse, skipErrorDialog?: boolean) {
    const err: ServerError = {
      message: '',
      errors: []
    };

    // Format error
    if (error instanceof HttpErrorResponse) {
      if (error.status === 0) {
        err.message = 'An error occured';
        err.errors.push('Could not connect to server, try again later');
      } else if (error.error && error.error.message) {
        err.message = `${error.status} - ${error.statusText}`;
        err.errors.push(error.error.message ? error.error.message : error.toString());
      } else {
        err.message = `${error.status} - ${error.statusText}`;
        err.errors.push(error.message ? error.message : error.toString());
      }
      if (!skipErrorDialog) {
        this.displayErrorDialog(err);
      }

    } else {
      err.message = 'Unknown error';
      err.errors.push(JSON.stringify(error, null, 2));
      if (!skipErrorDialog) {
        this.displayErrorDialog(err);
      }
    }

    return err;
  }

  private displayErrorDialog(err: ServerError) {
    let text = '';
    for (const error of err.errors) {
      text += `${error}<br/>`;
    }
    this.dialogService.defaultDialog(
      err.message,
      text,
      '',
      [{
        text: 'Ok',
        action: 'cancel'
      }]
    ).subscribe((action) => {
      //
    });
  }
}
