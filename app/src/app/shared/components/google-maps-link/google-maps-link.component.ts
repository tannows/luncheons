import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-google-maps-link',
  template: `
    <ng-content></ng-content>
    <a *ngIf="link" [href]="link" target="_blank"><i class="icon_pin"></i></a>
  `,
  styleUrls: ['./google-maps-link.component.scss']
})
export class GoogleMapsLinkComponent {
  @Input()
  public address: string;
  @Input()
  public zipCode: string;
  @Input()
  public city: string;

  constructor() {
    //
  }

  public get link(): string {
    const url = 'https://www.google.com/maps/search/?api=1&query=';
    if (this.address && (this.zipCode || this.city)) {
      const address = encodeURI(this.address);
      const zipCode = this.zipCode ? `+${encodeURI(this.zipCode)}` : '';
      const city = this.city ? `+${encodeURI(this.city)}` : '';
      return `${url}${address},${zipCode}${city}`;
    }
    return null;
  }
}
