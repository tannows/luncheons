import { Pipe, PipeTransform } from '@angular/core';
import { Restaurant, Lunch } from '@shared/interfaces';

@Pipe({
  name: 'restaurantAvailable'
})
export class RestaurantAvailablePipe implements PipeTransform {

  public transform(
    restaurants: Restaurant[],
    lunches: Lunch[],
    notAvailable?: boolean,
    lunchException?: Lunch
  ): any[] {
  	if (!restaurants || !lunches) {
  		return [];
  	}
  	const ids = [];
  	for (const lunch of lunches) {
      if (!lunch.inactive && lunch.restaurant) {
        ids.push(lunch.restaurant._id);
      }
  	}
  	const filtered = restaurants.filter((restaurant) => {
      const visited = ids.indexOf(restaurant._id) !== -1;
      const isException = lunchException
                       && lunchException.restaurant
                       && lunchException.restaurant._id === restaurant._id ? true : false;
      return (!notAvailable && (!visited || isException || restaurant.bot))
          || (notAvailable && visited && !restaurant.bot);
    });
    return filtered;
  }
}
