import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'active'
})
export class ActivePipe implements PipeTransform {

  public transform(array: any[], reverse?: boolean): any[] {
  	if (!array) {
  		return [];
  	}

    const filtered = array.filter((obj) => {
      return (!reverse && !obj.inactive) || (reverse && obj.inactive);
    });

    return filtered;
  }
}
