import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  public transform(array: any[], prop: string | string[], reverse?: boolean): any[] {
    if (!array) {
      return [];
    }
    array.sort((a: any, b: any) => {
      let _a;
      let _b;
      if (Array.isArray(prop)) {
        for (const p of prop) {
          if (a[p] && b[p]) {
            a = a[p];
            b = b[p];
          }
        }
      } else {
        a = a[prop];
        b = b[prop];
      }

      if (this.isDate(a) && this.isDate(b)) {
        _a = this.getCorrectDate(a);
        _b = this.getCorrectDate(b);
      } else {
        _a = this.isNumber(a) ? +a : a;
        _b = this.isNumber(b) ? +b : b;
      }

      if (_a < _b) {
        return -1;
      } else if (_a > _b) {
        return 1;
      } else {
        return 0;
      }
    });

    if (reverse) {
      array.reverse();
    }

    return array;
  }

  private getCorrectDate(value: any): number {
    return Date.parse(value);
  }

  private isNumber(value: any): boolean {
    return value && !Number.isNaN(+value) && Number.isInteger(+value);
  }

  private isDate(value: any): boolean {
    return this.isNumber(Date.parse(value));
  }
}
