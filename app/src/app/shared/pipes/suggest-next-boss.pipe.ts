import { Pipe, PipeTransform } from '@angular/core';
import { Boss, Lunch } from '@shared/interfaces';
import { OrderByPipe } from './order-by.pipe';
import { ActivePipe } from './active.pipe';

@Pipe({
  name: 'suggestNextBoss'
})
export class SuggestNextBossPipe implements PipeTransform {

  constructor(
    private orderByPipe: OrderByPipe,
    private activePipe: ActivePipe
  ) {
    //
  }

  public transform(bosses: Boss[], lunches: Lunch[]): Boss {
  	if (!bosses || !lunches) {
  		return null;
  	}
    // Sort bosses and lunches
    bosses = this.activePipe.transform(bosses);
    lunches = this.orderByPipe.transform(lunches, 'date', true);
    // Remove bot bosses
    bosses = bosses.filter((boss) => !boss.bot);

    const bossIds = [];
    bosses.forEach((boss) => bossIds.push(boss._id));

    const lunchesBossIds = [];
    let nextBossId = null;

    lunches.forEach((lunch) => {
      if (bossIds.includes(lunch.boss._id) && !lunchesBossIds.includes(lunch.boss._id)) {
        lunchesBossIds.push(lunch.boss._id);
      }
    });

    // First check if someone hasn't had their turn
    if (lunchesBossIds.length !== bossIds.length) {
      const delta = this.diff(bossIds, lunchesBossIds);
      // Pick the first of the one(s) that hasn't had their turn
      nextBossId = delta[0];
    } else {
      // If everyone has had their turn once, take the last from lunchesBossIds
      nextBossId = lunchesBossIds[lunchesBossIds.length - 1];
    }

    return bosses.find((boss) => boss._id === nextBossId);
  }

  private diff(a: string[], b: string[]): string[] {
    return a.filter((i) => b.indexOf(i) < 0);
  }
}
