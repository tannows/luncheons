import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'closestToNow'
})
export class ClosestToNowPipe implements PipeTransform {

  public transform(array: any[]): any[] {
  	if (!array) {
  		return [];
  	}

    const today = new Date();
    today.setHours(0, 0, 0, 0);

    const filtered = array.filter((obj) => {
      const objDate = new Date(Date.parse(obj.date));
      return objDate >= today;
    });

    return filtered;
  }
}
