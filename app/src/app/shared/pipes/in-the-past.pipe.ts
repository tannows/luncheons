import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'inThePast'
})
export class InThePastPipe implements PipeTransform {

  public transform(obj: any): boolean {
  	if (!obj) {
  		return obj;
  	}
  	const today = new Date();
  	today.setHours(0, 0, 0, 0);
  	const objDate = new Date(Date.parse(obj.date));
  	return objDate < today;
  }
}
