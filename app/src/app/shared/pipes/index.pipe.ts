import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'index'
})
export class IndexPipe implements PipeTransform {

  public transform(array: any[], obj: any): any {
  	if (!array || !obj) {
  		return;
  	}
    return array.findIndex(o => o._id === obj._id) + 1;
  }
}
