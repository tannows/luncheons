export interface Boss {
	_id: string;
	__v: number;
	name: string;
	createdAt: string;
	updatedAt: string;
	inactive: boolean;
	bot: boolean;
}
