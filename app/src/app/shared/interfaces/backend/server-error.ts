export interface ServerError {
	message: string;
	errors: string[];
}
