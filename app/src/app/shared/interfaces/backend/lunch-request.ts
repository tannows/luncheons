export interface LunchRequest {
	date: string;
	time: string;
	theme: string;
	restaurantId: string;
	bossId: string;
	inactive: boolean;
	takeaway: boolean;
}
