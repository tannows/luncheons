export interface LunchAttendantRequest {
	bossId: string;
	lunchId: string;
	attending: boolean;
}
