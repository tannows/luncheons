export interface BossRequest {
	name: string;
	inactive: boolean;
	bot: boolean;
}
