export interface RestaurantRequest {
	name: string;
	address: string;
	city: string;
	zipCode: string;
	menuLink: string;
	inactive: boolean;
	bot: boolean;
}
