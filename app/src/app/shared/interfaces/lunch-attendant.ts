import { Boss } from './boss';
import { Lunch } from './lunch';

export interface LunchAttendant {
	_id: string;
	boss: Boss;
	lunch: Lunch;
	createdAt: string;
	updatedAt: string;
}
