export interface Restaurant {
	_id: string;
	__v: number;
	name: string;
	address: string;
	city: string;
	zipCode: string;
	menuLink: string;
	createdAt: string;
	updatedAt: string;
	inactive: boolean;
	bot: boolean;
}
