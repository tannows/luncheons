interface LunchRestaurant {
	_id: string;
	name: string;
	address: string;
}

interface LunchBoss {
	_id: string;
	name: string;
}

export interface Lunch {
	_id: string;
	__v: number;
	date: string;
	time: string;
	theme: string;
	boss: LunchBoss;
	restaurant: LunchRestaurant;
	createdAt: string;
	updatedAt: string;
	inactive: boolean;
	takeaway: boolean;
	takeawayTo: LunchRestaurant;
}
