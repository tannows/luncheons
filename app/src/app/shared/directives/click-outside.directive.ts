import {
  Directive,
  ElementRef,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';

/*
 * Directive to catch if user is clicking outside host element or other given
 * click targets and firing appropriate event accordingly
 */

@Directive({
  selector: '[appClickOutside]'
})
export class ClickOutsideDirective {
  @Input()
  public clickTargets: any[];
  @Input()
  public skipIfHidden: boolean;
  @Output()
  public clickedOutside = new EventEmitter();

  constructor(
    private elementRef: ElementRef
  ) {
    //
  }

  @HostListener('document:click', ['$event'])
  private onClick(event: any) {
    const targetElement = event.target;
    if (this.skipIfHidden && !this.elementRef.nativeElement.classList.contains('visible')) {
      return;
    }
    const clickedInside = targetElement === this.elementRef.nativeElement
      || this.clickedOnOtherTagets(targetElement)
      || this.elementRef.nativeElement.contains(targetElement);

    if (!clickedInside) {
      this.clickedOutside.emit(event);
    }
  }

  private clickedOnOtherTagets(targetElement: any) {
    if (this.clickTargets) {
      for (const element of this.clickTargets) {
        if (element === targetElement) {
          return true;
        }
      }
    }
    return false;
  }
}
